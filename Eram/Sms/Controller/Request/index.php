<?php
/**
 * Eram Sms Controller for Testing.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */

namespace Eram\Sms\Controller\Request;

use Eram\Sms\Model\Sms;

class Index extends \Magento\Framework\App\Action\Action{

	/**
	 * Execute
	 * @return [type] [description]
	 */
	public function execute(){
		$sms = $this->_objectManager->get('Eram\Sms\Model\Sms');
		$sms->phoneNumber = '1234567890';
		$smsResponse = $sms->orderConfirmSms();		
	}

}