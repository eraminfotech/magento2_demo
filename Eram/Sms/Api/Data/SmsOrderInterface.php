<?php
/**
 * Eram Sms.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */
namespace Eram\Sms\Api\Data;

/**
 * SmsOrder Interface
 * @api
 */
interface SmsOrderInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID     = 'entity_id';
    const UPDATE_AT     = 'updated_at';
    const CREATED_AT    = 'created_at';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\Marketplace\Api\Data\FeedbackInterface
     */
    public function setId($id);

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);

    /**
     * Set UpdatedAt.
     */
    public function setUpdatedAt($updateAt);
}
