<?php
/**
 * Eram Sms Base Class.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */
namespace Eram\Sms\Model;

Class Sms
{
	
	/**
	 * \Eram\Sms\Helper\Data $dataHelper
	 * @var [type]
	 */
	protected $_dataHelper;
	/**
	 * \Eram\Sms\Helper\Data $dataHelper
	 * @var [type]
	 */
	protected $_objectManager;

	/**
	 * [$phoneNumber description]
	 * @var [type]
	 */
	public $phoneNumber;

	/**
	 * [$postData description]
	 * @var [type]
	 */
	protected $postData;

	/**
	 * [$_providerClass description]
	 * @var [type]
	 */
	protected $_providerClass;

	/**
	 * @var  \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory
	 */
	protected $_curl;
	/**
	 * @var  \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory
	 */
	protected $_curlFactory;

	/**
	 * @var \Magento\Store\Model\StoreManagerInterface $storeManager,
	 */
	protected $_storeManager;

	/**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    public $orderId;
    public $customerId;
    public $orderStatus;
    public $storeId;
    public $customerName;
    public $storeName;
    public $totalItemCount;
    public $allItems;
    public $firstProductName;

	public function __construct(
		\Eram\Sms\Helper\Data $dataHelper,
		\Magento\Framework\ObjectManagerInterface $objectManager,
		\Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Psr\Log\LoggerInterface $logger
	)
	{

		$this->_curlFactory     = $curlFactory;
		$this->_storeManager 	= $storeManager;
		$this->_logger 			= $logger;
		
		//start
		$this->_dataHelper 		= $dataHelper;
		$this->_objectManager 	= $objectManager;

		if(! $this->_dataHelper->allowExtension() )
			return;

		$this->_SmsProviderClass =	$this->_objectManager->get($this->_dataHelper->getProviderModel());
	}
	
	/**
	 * [orderConfirmSms description]
	 * @return [type] [description]
	 */
	public function orderConfirmSms(){
		$this->getOrderConfirmMessage();
		$response = $this->send();
		return $this->smsResponse($response);
	}

	/**
	 * [orderCancellationSms description]
	 * @return [type] [description]
	 */
	public function orderCancellationSms(){
		$this->getOrderCancellationMessage();
		$response = $this->send();
		return $this->smsResponse($response);
	}

	/**
	 * [orderShippedSms description]
	 * @return [type] [description]
	 */
	public function orderShippedSms(){
		$this->getOrderShippedMessage();
		$response = $this->send();
		return $this->smsResponse($response);
	}

	/**
	 * [getOrderConfirmMessage description]
	 * @return [type] [description]
	 */
	private function getOrderConfirmMessage(){
		$this->postData = $this->_SmsProviderClass->makeData(
		$this->_dataHelper->getOrderConfirmationSms($this->totalItemCount,$this->firstProductName,$this->storeName),
				$this->phoneNumber
				);	
	}

	/**
	 * [getOrderCancellationMessage description]
	 * @return [type] [description]
	 */
	private function getOrderCancellationMessage(){
		$this->postData = $this->_SmsProviderClass->makeData(
		$this->_dataHelper->getOrderCancellationSms($this->totalItemCount,$this->firstProductName,$this->storeName),
				$this->phoneNumber
				);	
	}

	/**
	 * [getOrderShippedMessage description]
	 * @return [type] [description]
	 */
	private function getOrderShippedMessage(){
		$this->postData = $this->_SmsProviderClass->makeData(
		$this->_dataHelper->getOrderShippedSms($this->totalItemCount,$this->firstProductName,$this->storeName),
				$this->phoneNumber
				);	
	}

	/**
	 * [smsResponse description]
	 * @param  [type] $response [description]
	 * @return [type]           [description]
	 */
	private function smsResponse($response){
		if( isset( $response['response'] ) && ! empty( $response['response'] ) ){
			
			$this->_SmsProviderClass->orderId 		= $this->orderId;
			$this->_SmsProviderClass->customerId 	= $this->customerId;
			$this->_SmsProviderClass->orderStatus 	= $this->orderStatus;
			$this->_SmsProviderClass->storeId 		= $this->storeId;

			$this->_SmsProviderClass->parseResponse($response['response']);
			return $this->_SmsProviderClass->prepareOrderData();
		}
		elseif(isset( $response['http_error'])) {
			$this->_logger->critical("SMS gateway http error");
		}
	}

	/**
     * Api certificate getter
     *
     * @return string
     */
    public function getApiCertificate()
    {
        $websiteId = $this->_storeManager->getStore($this->_storeId)->getWebsiteId();
        return $this->_certFactory->create()->loadByWebsite($websiteId, false)->getCertPath();
    }

    /**
     * [send description]
     * @return [type] [description]
     */
	private function send(){
        try {
            $http = $this->_curlFactory->create();
            $config = ['timeout' => 60];
            
            $http->setConfig($config);
            $http->write(
                \Zend_Http_Client::POST,
                $this->_SmsProviderClass->api_endpoint,
                '1.1',
                null,
                http_build_query($this->postData)
            );
            $response = $http->read();
        } catch (\Exception $e) {
            $debugData['http_error'] = ['error' => $e->getMessage(), 'code' => $e->getCode()];
            throw $e;
        }

        $response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);

        $debugData['response'] = json_decode($response);
        
        if ($http->getErrno()) {
            $this->_logger->critical(
                new \Exception(
                    sprintf('SMS gateway connection error #%s: %s', $http->getErrno(), $http->getError())
                )
            );
            $http->close();

        }

        // cUrl resource must be closed after checking it for errors
        $http->close();

        return $debugData;
	}
}