<?php
/**
 * Eram Sms Model Config Class.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */
namespace Eram\Sms\Model\Config;

class Config{

	/**
	 * @var $orderId
	 */
	public $orderId;
	/**
	 * @var $orderStatus
	 */
	public $orderStatus;
	/**
	 * @var $providerId
	 */
	public $providerId = 1;
	/**
	 * @var $providerMsgId
	 */
	public $providerMsgId;
	/**
	 * @var $customerId
	 */
	public $customerId;
	/**
	 * @var $storeId
	 */
	public $storeId;
	/**
	 * @var $errorCode
	 */
	public $errorCode;
	/**
	 * @var $msgData
	 */
	public $msgData;
	/**
	 * @var $errorMessage
	 */
	public $errorMessage;
	/**
	 * @var $status
	 */
	public $status;

	 /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    public function __construct(\Magento\Framework\Stdlib\DateTime\DateTime $date){
    	$this->_date = $date;
    }

    /**
     * [getOrderId description]
     * @return [type] [description]
     */
	protected function getOrderId(){
		return $this->orderId;
	}

	/**
	 * [getOrderStatus description]
	 * @return [type] [description]
	 */
	protected function getOrderStatus(){
		return $this->orderStatus;
	}

	/**
	 * [getProviderId description]
	 * @return [type] [description]
	 */
	protected function getProviderId(){
		return $this->providerId;
	}

	/**
	 * [getProviderMsgId description]
	 * @return [type] [description]
	 */
	protected function getProviderMsgId(){
		return $this->providerMsgId;
	}

	/**
	 * [getCustomerId description]
	 * @return [type] [description]
	 */
	protected function getCustomerId(){
		return $this->customerId;
	}

	/**
	 * [getStorerId description]
	 * @return [type] [description]
	 */
	protected function getStorerId(){
		return $this->storeId;
	}

	/**
	 * [getErrorCode description]
	 * @return [type] [description]
	 */
	protected function getErrorCode(){
		return $this->errorCode;
	}

	/**
	 * [getMsgData description]
	 * @return [type] [description]
	 */
	protected function getMsgData(){
		return $this->msgData;
	}

	/**
	 * [getErrorMsg description]
	 * @return [type] [description]
	 */
	protected function getErrorMsg(){
		return $this->errorMessage;
	}

	/**
	 * [getStatus description]
	 * @return [type] [description]
	 */
	protected function getStatus(){
		return $this->status;
	}
}