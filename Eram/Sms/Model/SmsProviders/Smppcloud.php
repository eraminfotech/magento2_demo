<?php
/**
 * Eram Sms Smpp Base Class.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */
namespace Eram\Sms\Model\SmsProviders;

Class Smppcloud extends \Eram\Sms\Model\Config\Config
{

	/**
	 * @var $api_endpoint
	 */
	public $api_endpoint;
	/**
	 * @var $user
	 */
	public $user;
	/**
	 * @var $password
	 */
	public $password;
	/**
	 * @var $sid
	 */
	public $sid;
	/**
	 * @var $fl
	 */
	public $fl;
	/**
	 * @var $gwid
	 */
	public $gwid;

	//response;
	/**
	 * @var $ErrorCode
	 */
	public $ErrorCode;
	/**
	 * @var $ErrorMessage
	 */
	public $ErrorMessage;
	/**
	 * @var $JobId
	 */
	public $JobId;
	/**
	 * @var $MessageData
	 */
	public $MessageData;

	//
	private $env_mode;
	private $env_var;
	private $default_phonenumber;

	public function __construct(){
		$this->api_endpoint = '';
		$this->user 		= '';
		$this->password		= '';
		$this->sid 			=  '';
		$this->fl 			= '';
		$this->gwid 		= '';

		$this->env_mode      		=  "production";
        $this->env_var       		=  "APPLICATION_ENV";
        $this->default_phonenumber 	=  "1234567890";

	}

	/**
	 * [makeData description]
	 * @param  [type] $msg          [description]
	 * @param  [type] $phone_number [description]
	 * @return [type]               [description]
	 */
	public function makeData($msg,$phone_number){
		$d = array();
		$d['user'] 		= 	$this->user;
		$d['password'] 	= 	$this->password;
		$d['sid'] 		= 	$this->sid;
		$d['fl'] 		= 	$this->fl;
		$d['gwid']		=	$this->gwid;
		$d['msisdn'] 	= 	$this->getPhoneNumber($phone_number);
		$d['msg'] 		= 	$msg;
		return $d;
	}

	/**
	 * [prepareOrderData description]
	 * @return [type] [description]
	 */
	public function prepareOrderData(){
		$d = array();
		$d['order_id'] 			= $this->getOrderId();
		$d['order_status'] 		= $this->getOrderStatus();
		$d['provider_id'] 		= $this->getProviderId();
		$d['provider_msg_id'] 	= $this->getProviderMsgId();
		$d['customer_id'] 		= $this->getCustomerId();
		$d['store_id'] 			= $this->getStorerId();
		$d['error_code'] 		= $this->getErrorCode();
		$d['message_data'] 		= serialize($this->getMsgData());
		$d['error_message'] 	= $this->getErrorMsg();
		$d['status'] 			= $this->getStatus();
		return $d;
	}	

	/**
	 * [parseErrorCode description]
	 * @param  [type] $errorCode [description]
	 * @return [type]            [description]
	 */
	private function parseErrorCode($errorCode){
		if( '000' === $errorCode ){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * [parseResponse description]
	 * @param  [type] $reponse [description]
	 * @return [type]          [description]
	 */
	public function parseResponse($reponse){
		$this->providerMsgId = $reponse->JobId;
		$this->errorCode 	= $reponse->ErrorCode;
		$this->msgData 		= $reponse->MessageData;
		$this->errorMessage = $reponse->ErrorMessage;
		$this->status 		= $this->parseErrorCode($reponse->ErrorCode);
	}

	/**
	 * [getPhoneNumber description]
	 * @param  [type] $phoneNumber [description]
	 * @return [type]              [description]
	 */
	private function getPhoneNumber($phoneNumber){
		if( getenv($this->env_var) === false || getenv($this->env_var) != $this->env_mode ){
			return $this->default_phonenumber;
		}
		else{
			return $phoneNumber;
		}
	}
}	