<?php
/**
 * Eram Sms Model Class.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */

namespace Eram\Sms\Model;

class SmsOrder extends \Magento\Framework\Model\AbstractModel implements \Eram\Sms\Api\Data\SmsOrderInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'sms_order_log';

    protected function _construct()
    {
        $this->_init('Eram\Sms\Model\ResourceModel\SmsOrder');
    }

    /**
     * [getIdentities description]
     * @return [type] [description]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * [_beforeSave description]
     * @return [type] [description]
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        /**
         * Perform some actions just before a brand is saved.
         */
        $this->_updateTimestamps();

        return $this;
    }

    /**
     * [_updateTimestamps description]
     * @return [type] [description]
     */
    protected function _updateTimestamps()
    {
        $timestamp = now();
        /**
         * Set the last updated timestamp.
         */
        $this->setUpdatedAt($timestamp);

        /**
         * If we have a brand new object, set the created timestamp.
         */
        if ($this->isObjectNew()) {
            $this->setCreatedAt($timestamp);
        }

        return $this;
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set UpdateTime.
     */
    public function setUpdatedAt($updateAt)
    {
        return $this->setData(self::UPDATE_AT, $updateAt);
    }
}