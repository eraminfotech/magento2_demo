<?php
/**
 * Eram Sms Model Resource Collection.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */
namespace Eram\Sms\ResourceModel\SmsOrder;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Eram\Sms\Model\SmsOrder','Eram\Sms\Model\ResourceModel\SmsOrder');
    }
}
