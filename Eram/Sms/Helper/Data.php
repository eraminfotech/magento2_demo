<?php
/**
 * Eram Sms Helper Class.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */
namespace Eram\Sms\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Catalog data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * ScopeConfigInterface scopeConfig
     *
     * @var scopeConfig
     */
    protected $scopeConfig;
     
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
        ) {
        parent::__construct($context);
    }

    /**
     * Check ectension
     * @return [type] [description]
     */
    public function allowExtension(){
        return  $this->scopeConfig->getValue('eramsms/general/enable_eram_sms', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }  

    /**
     * getSmsProvider
     * @return [type] [description]
     */
    public function getSmsProvider(){
        //return  $this->scopeConfig->getValue('eramsms/general/enable_eram_sms', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        return '\Smppcloud';
    }

    /**
     * getProviderModel
     * @return [type] [description]
     */
    public function getProviderModel(){
        return "\Eram\Sms\Model\SmsProviders".$this->getSmsProvider();
    }

    /**
     * [getOrderConfirmationSms description]
     * @param  [type] $productCount [description]
     * @param  [type] $productName  [description]
     * @param  [type] $storeName    [description]
     * @return [type]               [description]
     */
    public function getOrderConfirmationSms($productCount,$productName,$storeName){
        $str = ($productCount > 1) ? "and %s other items (s)" : "%s";
        $productCount = ($productCount > 1) ? $productCount - 1 : "";

        $cnt = ($productCount > 1) ? 25 : 50;

        return sprintf("Your order for %s ".$str." has been successfully placed. Check email for details. Thank you for shopping at %s",$this->truncateString($productName,$cnt),$productCount,$storeName);
    }

    /**
     * [getOrderCancellationSms description]
     * @param  [type] $productCount [description]
     * @param  [type] $productName  [description]
     * @param  [type] $storeName    [description]
     * @return [type]               [description]
     */
    public function getOrderCancellationSms($productCount,$productName,$storeName){
        $str = ($productCount > 1) ? "and %s other items (s)" : "%s";
        $productCount = ($productCount > 1) ? $productCount - 1 : "";
        $cnt = ($productCount > 1) ? 50 : 50;

        return sprintf("Your order for %s ".$str." has been cancelled. Check email for details. Thank you for shopping at %s",$this->truncateString($productName,$cnt),$productCount,$storeName);
    }

    /**
     * [getOrderShippedSms description]
     * @param  [type] $productCount [description]
     * @param  [type] $productName  [description]
     * @param  [type] $storeName    [description]
     * @return [type]               [description]
     */
    public function getOrderShippedSms($productCount,$productName,$storeName){
        //Dispatched: Your package with [product name..] has been dispatched. Will be delivered on or before [estimated date] 
        $str = ($productCount > 1) ? "and %s other items (s)" : "%s";
        $productCount = ($productCount > 1) ? $productCount - 1 : "";
        $cnt = ($productCount > 1) ? 50 : 50;

        return sprintf("Your package with %s ".$str." has been dispatched. Check email for details. Thank you for shopping at %s",$this->truncateString($productName,$cnt),$productCount,$storeName);
    }

    /**
     * [truncateString description]
     * @param  [type]  $string [description]
     * @param  integer $count  [description]
     * @return [type]          [description]
     */
    private function truncateString($string,$count = 50){
        return (strlen($string) > $count) ? substr($string, 0, $count) . '...' : $string;
    }
}
