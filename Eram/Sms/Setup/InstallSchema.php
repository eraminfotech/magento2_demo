<?php
/**
 * Eram Sms InstallSchema Class.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */

namespace Eram\Sms\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
       
    }
}
