<?php
/**
 * Eram Sms Observer CheckOutOnePageSuccessAfter Class.
 *
 * @category  Sms
 * @package   Eram_Sms
 * @author    Eraminfotech
 * @copyright Copyright (c) 2010-2017 Eraminfotech Pvt Ltd
 */
namespace Eram\Sms\Observer\Sales;

use Magento\Sales\Model\OrderFactory;


class CheckOutOnePageSuccessAfter implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    protected $_logger;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var orderId
     */
    private $orderId;
    /**
     * @var customerId
     */
    private $customerId;
    /**
     * @var orderStatus
     */
    private $orderStatus;
    /**
     * @var orderState
     */
    private $orderState;
    /**
     * @var storeId
     */
    private $storeId;
    /**
     * @var phoneNumber
     */
    private $phoneNumber;
    /**
     * @var customerName
     */
    private $customerName;
    /**
     * @var storeName
     */
    private $storeName;
    /**
     * @var totalItemCount
     */
    private $totalItemCount;
    /**
     * @var allItems
     */
    private $allItems;
    /**
     * @var firstProductName
     */
    private $firstProductName;

    /**
     * [$smsObj description]
     * @var [type]
     */
    private $smsObj;

    private $_orderFactory;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        OrderFactory $orderFactory,
        \Psr\Log\LoggerInterface $logger, //log injection
        array $data = []
    ) {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($writer);

        $this->_objectManager   = $objectManager;
        $this->_orderFactory    = $orderFactory;
        $this->_date = $date;

    }

    /**
     * [execute description]
     * @param  \Magento\Framework\Event\Observer $observer [description]
     * @return [type]                                      [description]
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {

        $lastOrderId = $observer->getEvent()->getData('order_ids');
        $order = $this->_orderFactory->create()->load($lastOrderId[0]);
        if (!$order->getId()) {
            return $this;
        }

        $this->orderId            = $order->getIncrementId();
        $this->customerId         = $order->getCustomerId();
        $this->orderStatus        = $order->getStatus();
        $this->orderState         = $order->getState();
        $this->storeId            = $order->getStoreId();
        $delivery_address         = $order->getShippingAddress()->getData();
        $this->phoneNumber        = $delivery_address['telephone'];
        $this->customerName       = $order->getCustomerName();
        $this->storeName          = $order->getStore()->getFrontendName();
        $this->totalItemCount     = $order->getTotalItemCount();
        $this->allItems           = $order->getAllItems();
        //$this->logme();

        $cnt = 1;
        foreach ($order->getAllItems() as $item) {
            if( $cnt == 1 ){
                $this->firstProductName = $item->getName();
            }
            $cnt ++;
        }

        //send sms from order confirmation
        //if( $order->getState() == \Magento\Sales\Model\Order::STATE_PROCESSING ){
        if( $order->getStatus() == "payment_confirmed" ){
            $this->smsObj = $this->_objectManager->get('Eram\Sms\Model\Sms');
            $this->setSmsObj();
            $this->sendOrderConfirmationSms();           
        }

    }

    private function setSmsObj(){
        $this->smsObj->phoneNumber   = $this->phoneNumber;
        $this->smsObj->orderId       = $this->orderId;
        $this->smsObj->customerId    = $this->customerId;
        $this->smsObj->orderStatus   = $this->orderStatus;
        $this->smsObj->storeId       = $this->storeId;
        $this->smsObj->customerName  = $this->customerName;
        $this->smsObj->storeName     = $this->storeName;
        $this->smsObj->totalItemCount   =$this->totalItemCount;
        $this->smsObj->allItems         =$this->allItems;
        $this->smsObj->firstProductName         =$this->firstProductName;
    }

    private function sendOrderConfirmationSms(){
        $smsResponse        = $this->smsObj->orderConfirmSms();   
        $this->createSmsFactory($smsResponse);
    }

    private function createSmsFactory($smsResponse){
        if( ! empty($smsResponse) ){
            // save to db
            $smsOrderFactory = $this->_objectManager->create('Eram\Sms\Model\SmsOrderFactory');
            $todo = $smsOrderFactory->create();
            $smsResponse['created_at']        = $this->_date->gmtTimestamp();
            $smsResponse['updated_at']        = $this->_date->gmtTimestamp();
            $todo->setData($smsResponse)->save();
        }
    }

    private function logme(){

        $this->_logger->info("order id---->".$this->orderId);   
        $this->_logger->info("customer id---->".$this->customerId);
        $this->_logger->info("status id---->".$this->orderStatus);
        $this->_logger->info("state id---->".$this->orderState);
        //$this->_logger->info("telephone id---->".$delivery_address['telephone']);
        $this->_logger->info("stroeId id---->".$this->storeId);
        // $this->_logger->info("getShippingMethod id---->".$order->getShippingMethod());
        $this->_logger->info('getplace oreder event occured successfully!');
    }
}
